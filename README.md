#The Awesome Chat

######This chat application was made to showcase the simplicity of building an integrated system with all kinds of features.

The chat handles all user interaction in real-time, if the user allows it, has positional tracking of all users with implemented map location and weather information of every users location. 
Besides that it implements google translate, determines the users resident country, assumes the spoken language of the user and translates all foreign typed text coming from other users into the users own native tongue.

##Technology

* ###[Node.js](https://nodejs.org/)
     
    Used as the runtime envoirement for our JavaScript codebase.   
***

* ###[Socket.io](http://socket.io/)

    Used for the bi-directional communication between the users while chatting and emitting to everyone else when ever someone new have joined the chat or whenever someone quits the chat.
***

* ###[Express.js](http://expressjs.com/)

    Express is the framework used to serve the static files.
***

* ###[Mongoose.js](http://mongoosejs.com/)

    ODM used for handling the NoSQL database.
***

* ###[Bootstrap](http://getbootstrap.com/)

    Front-end framework used mainly for it's grid and responsive capabilities. 
***

* ###[Mongo-Express](http://andzdroid.github.io/mongo-express/)

    Web-based MongoDB admin interface used for exploring the documents. 
***

* ###[Country-Language](https://github.com/bdswiss/country-language)

    Module used for handling and getting country codes.
***

* ###[Assert](https://github.com/defunctzombie/commonjs-assert)

    An assertion unit test module that got implemented but never used.
***