// Setup basic express server
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('..')(server);
var countryLanguage = require('country-language');
var port = process.env.PORT || 3000;
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  // yay!
});
 
var messageSchema = mongoose.Schema({
	messageContent     	: String,
    username            : String
});

var Message = mongoose.model('Message', messageSchema);


server.listen(port, function () {
    console.log('Server listening at port %d', port);
});

// Routing
app.use(express.static(__dirname + '/public'));

// Chatroom

// usernames which are currently connected to the chat
var usernames = {};
var numUsers = 0;

var users = {};

io.on('connection', function (socket) {
    var addedUser = false;

    // when the client emits 'new message', this listens and executes
    socket.on('new message', function (data) {

        var message = new Message({ messageContent: data, username: socket.username });

        message.save(function (err, messageTest) {
          if (err) return console.error(err);
          console.log(message);
        });

        // we tell the client to execute 'new message'
        socket.broadcast.emit('new message', {
            username: socket.username,
            message: data
        });
    });

    // when the client emits 'add user', this listens and executes
    socket.on('add user', function (username, ip, long, lat, countrycode, color) {
        countrycode = countryLanguage.getCountry(countrycode).languages[0].iso639_1;
        //console.log(ip);
        // we store the username in the socket session for this client
        socket.username = username;
        // add the client's username to the global list

        users[username] = {username: username, ip: ip, long: long, lat: lat, countrycode: countrycode, color: color};
        console.log(users[username]);

        usernames[username] = username;
        ++numUsers;
        addedUser = true;
        socket.emit('login', {
            numUsers: numUsers,
            //users: usernames
            users: users
        });
        // echo globally (all clients) that a person has connected
        socket.broadcast.emit('user joined', {
            username: socket.username,
            numUsers: numUsers,
            //users: usernames
            users: users
        });
    });

    // when the client emits 'typing', we broadcast it to others
    socket.on('typing', function () {
        socket.broadcast.emit('typing', {
            username: socket.username
        });
    });

    // when the client emits 'stop typing', we broadcast it to others
    socket.on('stop typing', function () {
        socket.broadcast.emit('stop typing', {
            username: socket.username
        });
    });

    // when the user disconnects.. perform this
    socket.on('disconnect', function () {
        // remove the username from global usernames list
        if (addedUser) {
            delete usernames[socket.username];
            delete users[socket.username];
            --numUsers;

            // echo globally that this client has left
            socket.broadcast.emit('user left', {
                username: socket.username,
                numUsers: numUsers,
                //users: usernames
                users: users
            });
        }
    });
});
