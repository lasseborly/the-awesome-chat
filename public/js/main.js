$(function () {
    var FADE_TIME = 150; // ms
    var TYPING_TIMER_LENGTH = 400; // ms
    var COLORS = [
        '#e21400', '#91580f', '#f8a700', '#f78b00',
        '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
        '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
    ];

    // Initialize varibles
    var $window = $(window);
    var $usernameInput = $('.usernameInput'); // Input for username
    var $messages = $('.messages'); // Messages area
    var $inputMessage = $('.inputMessage'); // Input message input box
    var $usernames = $('.usernames');
    var $userIP = $('.userIP');
    var $ipField = $('#ip');
    var $weatherField = $('#weather');

    var $dropdown = $('#jq-dropdown-1');

    var $loginPage = $('.login.page'); // The login page
    var $chatPage = $('.chat.page'); // The chatroom page


    // Prompt for setting a username
    var username;
    var connected = false;
    var typing = false;
    var lastTypingTime;
    var $currentInput = $usernameInput.focus();

    var socket = io();

    var usersList;
    var ip;

    var map;
    var marker;
    var long;
    var lat;

    var countryCode;

    google.maps.event.addDomListener(window, 'load', initialize);

    function initialize() {
        navigator.geolocation.getCurrentPosition(function (position) {
            long = position.coords.longitude;
            lat = position.coords.latitude;
        });
    }

    function prepareWeather(username, long, lat) {
        var response = $.ajax({
            url: 'https://api.forecast.io/forecast/204cec5ab2ef500aec5ece3d3725afe3/' + lat + ',' + long + "?&units=si",
            dataType: "jsonp",
            contentType: "application/json",
            async: false,
            success: function (data) {
                data.username = username;
                setWeather(data);
                //console.log(data);
                //return data;
                //$weatherField.html("Condition: " + data.currently.summary + "<br>" + "Temperature: " + data.currently.temperature + "F<br>" + "Wind: " + data.currently.windSpeed + "Mph");
                //weatherString = "<br>Condition: " + data.currently.summary + "<br>" + "Temperature: " + data.currently.temperature + " C<br>" + "Wind: " + data.currently.windSpeed + " m/s"
                //console.log(data);
            }
        });
    }

    function setWeather(data) {
        var weatherString = "<h4>Weather on " + (data.username.substr(data.username.length - 1) === "s" ? data.username + "' " : data.username + "'s ") + " location</h4><b>Condition:</b> " + data.currently.summary + "<br>" + "<b>Temperature: </b>" + data.currently.temperature + " C<br>" + "<b>Wind: </b>" + data.currently.windSpeed + " m/s";

        var infowindow = new google.maps.InfoWindow({
            content: weatherString
        });

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
    }

    function prepareMap(username, long, lat) {
        var mapCanvas = document.getElementById('map-canvas');

        var myLatlng = new google.maps.LatLng(lat, long);

        var mapOptions = {
            center: myLatlng,
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map = new google.maps.Map(mapCanvas, mapOptions)

        marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: username
        });

        $("#localtionModalTitle").text("Location for " + username);
        $("#localtionModalTitle").append('<span class=text-muted> &mdash; click the red marker for weather information</span>');
    }

    function getCountryCode() {
        var url = "http://ip-api.com/json/" + ip;

        var response = $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            async: false
        }).responseText;

        response = $.parseJSON(response);

        return response.countryCode;
    }

    function translate(message, source, target) {
        //if (source === target) return message;

        //var url = "https://www.googleapis.com/language/translate/v2?key=AIzaSyAHfc0U-aORQxLhRGz9GKpKm7TIqPy0oDA&source=" + source + "&target=" + target + "&q=" + message;
        var url = "https://www.googleapis.com/language/translate/v2?key=AIzaSyAHfc0U-aORQxLhRGz9GKpKm7TIqPy0oDA&target=" + target + "&q=" + message;

        var response = $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            async: false
        }).responseText;

        response = $.parseJSON(response);

        console.log(response);

        return {text: response['data'].translations[0].translatedText, lang: response['data'].translations[0].detectedSourceLanguage};
    }

    function addParticipantsMessage(data) {
        var message = '';
        if (data.numUsers === 1) {
            message += "there's 1 participant";
        } else {
            message += "there are " + data.numUsers + " participants";
        }
        log(message);
    }

    // Sets the client's username
    function setUsername() {
        username = cleanInput($usernameInput.val().trim());

        // If the username is valid
        if (username) {
            $loginPage.fadeOut();
            $chatPage.show();
            $loginPage.off('click');
            $currentInput = $inputMessage.focus();

            // Tell the server your username and ip
            ip = getIpAddress();
            countryCode = getCountryCode();
            socket.emit('add user', username, ip, long, lat, countryCode, getUsernameColor());
        }
    }

    // Sends a chat message
    function sendMessage() {
        var message = $inputMessage.val();
        // Prevent markup from being injected into the message
        message = cleanInput(message);
        // if there is a non-empty message and a socket connection
        if (message && connected) {
            $inputMessage.val('');
            addChatMessage({
                username: username,
                message: message
            });
            // tell server to execute 'new message' and send along one parameter
            socket.emit('new message', message);
        }
    }

    // Log a message
    function log(message, options) {
        var $el = $('<li>').addClass('log').text(message);
        addMessageElement($el, options);
    }

    // Adds the visual chat message to the message list
    function addChatMessage(data, options) {
        var originaltext = data.message;
        var translation = translate(data.message, usersList[data.username].countrycode, countryCode);
        data.message = translation.text;

        // Don't fade the message in if there is an 'X was typing'
        var $typingMessages = getTypingMessages(data);
        options = options || {};
        if ($typingMessages.length !== 0) {
            options.fade = false;
            $typingMessages.remove();
        }

        var $usernameDiv = $('<span class="username"/>')
            .text(data.username)
            .css('color', usersList[data.username].color);
        var $messageBodyDiv = $('<span class="messageBody">')
            .text(data.message);

        if(data.lang !== 'undefined') $messageBodyDiv.append(' <small class="text-muted vcenter">(from: <abbr title="' + originaltext +'">' + translation.lang + '</abbr>)</small>');

        var typingClass = data.typing ? 'typing' : '';
        var $messageDiv = $('<li class="message"/>')
            .data('username', data.username)
            .addClass(typingClass)
            .append($usernameDiv, $messageBodyDiv);

        addMessageElement($messageDiv, options);
    }

    // Adds the visual chat typing message
    function addChatTyping(data) {
        data.typing = true;
        data.message = 'is typing';
        addChatMessage(data);
    }

    // Removes the visual chat typing message
    function removeChatTyping(data) {
        getTypingMessages(data).fadeOut(function () {
            $(this).remove();
        });
    }

    // Adds a message element to the messages and scrolls to the bottom
    // el - The element to add as a message
    // options.fade - If the element should fade-in (default = true)
    // options.prepend - If the element should prepend
    //   all other messages (default = false)
    function addMessageElement(el, options) {
        var $el = $(el);

        // Setup default options
        if (!options) {
            options = {};
        }
        if (typeof options.fade === 'undefined') {
            options.fade = true;
        }
        if (typeof options.prepend === 'undefined') {
            options.prepend = false;
        }

        // Apply options
        if (options.fade) {
            $el.hide().fadeIn(FADE_TIME);
        }
        if (options.prepend) {
            $messages.prepend($el);
        } else {
            $messages.append($el);
        }
        $messages[0].scrollTop = $messages[0].scrollHeight;
    }

    // Prevents input from having injected markup
    function cleanInput(input) {
        return $('<div/>').text(input).text();
    }

    // Updates the typing event
    function updateTyping() {
        if (connected) {
            if (!typing) {
                typing = true;
                socket.emit('typing');
            }
            lastTypingTime = (new Date()).getTime();

            setTimeout(function () {
                var typingTimer = (new Date()).getTime();
                var timeDiff = typingTimer - lastTypingTime;
                if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
                    socket.emit('stop typing');
                    typing = false;
                }
            }, TYPING_TIMER_LENGTH);
        }
    }

    // Gets the 'X is typing' messages of a user
    function getTypingMessages(data) {
        return $('.typing.message').filter(function (i) {
            return $(this).data('username') === data.username;
        });
    }

    // Gets the color of a username through our hash function
    function getUsernameColor() {
        // Much simpler...
        return COLORS[Math.floor(Math.random() * COLORS.length)];

        // Compute hash code
        //var hash = 7;
        //for (var i = 0; i < username.length; i++) {
        //    hash = username.charCodeAt(i) + (hash << 5) - hash;
        //}
        //// Calculate color
        //var index = Math.abs(hash % COLORS.length);
        //return COLORS[index];
    }

    function updateUserList(users) {
        usersList = users;

        //console.log(usersList);

        $usernames.empty();
        $usernames.append('Users: ');

        var x = 0;
        var y = Object.keys(usersList).length;
        for (var user in usersList) {
            var u = usersList[user];
            $usernames.append($('<span />').attr({'class': 'userdropdown label label-primary', 'id': u.username, 'data-jq-dropdown': '#jq-dropdown'}).html(u.username));
            x++;
            if (x !== y) $usernames.append(' &mdash; ');
        }
    }

    function getIpAddress() {
        var ip = $.ajax({type: "GET", url: "http://46.101.165.19/ip/", async: false}).responseText;
        return ip.trim();
    }

    //Inserts the selected users IP in the dropdown menu!
    $('.usernames').on('click', '.userdropdown', function () {
        var username = $(this).attr("id");

        for (var user in usersList) {
            user = usersList[user];
            if (username === user.username) {
                $ipField.html('<a href="http://ip-api.com/' + user.ip + '" target="_blank">' + user.ip + '</a>');
                prepareWeather(user.username, user.long, user.lat);
                prepareMap(username, user.long, user.lat);
            }
        }
    });

    // Keyboard events

    $window.keydown(function (event) {
        // Auto-focus the current input when a key is typed
        if (!(event.ctrlKey || event.metaKey || event.altKey)) {
            $currentInput.focus();
        }
        // When the client hits ENTER on their keyboard
        if (event.which === 13) {
            if (username) {
                sendMessage();
                socket.emit('stop typing');
                typing = false;
            } else {
                setUsername();
            }
        }
    });

    $inputMessage.on('input', function () {
        updateTyping();
    });

    // Click events

    // Focus input when clicking anywhere on login page
    $loginPage.click(function () {
        $currentInput.focus();
    });

    // Focus input when clicking on the message input's border
    $inputMessage.click(function () {
        $inputMessage.focus();
    });

    // When the modal is shown, we must trigger a 'resize' event to make it draw correctly within the dynamic element (the modal)
    $('#locationModal').on('shown.bs.modal', function () {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });

    // Socket events

    // Whenever the server emits 'login', log the login message
    socket.on('login', function (data) {
        countryCode = data.users[username].countrycode;
        connected = true;
        // Display the welcome message
        var message = "Welcome to the awesome chat – ";
        log(message, {
            prepend: true
        });

        console.log(data.users);

        updateUserList(data.users);
        addParticipantsMessage(data);
    });

    // Whenever the server emits 'new message', update the chat body
    socket.on('new message', function (data) {
        addChatMessage(data);
    });

    // Whenever the server emits 'user joined', log it in the chat body
    socket.on('user joined', function (data) {
        log(data.username + ' joined');
        updateUserList(data.users);
        addParticipantsMessage(data);
    });

    // Whenever the server emits 'user left', log it in the chat body
    socket.on('user left', function (data) {
        log(data.username + ' left');
        updateUserList(data.users);
        addParticipantsMessage(data);
        removeChatTyping(data);
    });

    // Whenever the server emits 'typing', show the typing message
    socket.on('typing', function (data) {
        addChatTyping(data);
    });

    // Whenever the server emits 'stop typing', kill the typing message
    socket.on('stop typing', function (data) {
        removeChatTyping(data);
    });
});
